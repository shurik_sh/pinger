### Setup development

```shell
make init
```

I use dip for working with docker. You can see list of available commands:

```shell
dip ls
```
