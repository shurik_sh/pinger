class CreateHosts < ActiveRecord::Migration[6.1]
  def change
    create_table :hosts do |t|
      t.string :ip

      t.timestamps
    end
    add_index :hosts, :ip, unique: true
  end
end
