class CreateStats < ActiveRecord::Migration[6.1]
  def change
    create_table :stats do |t|
      t.references :host
      t.float :duration
      t.boolean :received, null: false

      t.timestamps
    end
  end
end
