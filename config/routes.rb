Rails.application.routes.draw do
  root "api/hosts#index"

  namespace :api do
    resources :hosts, only: %i[create index destroy] do
      member do
        get :stats
      end
    end
  end
end
