describe PingStats::Ping do
  let(:pinger) { instance_double(Net::Ping::ICMP, :ping => result, :duration => duration, "host=" => ip) }
  let(:service) { described_class.new(ip) }
  let(:result) { true }
  let(:duration) { 100 } # seconds
  let(:ip) { "8.8.8.8" }

  before do
    Container.enable_stubs!
    Container.stub("ping.driver", pinger)
  end

  after do
    Container.unstub("ping.driver")
  end

  describe ".new" do
    it "configure driver" do
      expect(pinger).to receive("host=").with(ip).once
      service
    end
  end

  describe "#ping" do
    it "sends ping" do
      expect(pinger).to receive(:ping).twice
      service.ping
      service.ping
    end

    it "returns hash result" do
      expect(service.ping).to have_attributes success?: true, duration: duration
    end
  end
end
