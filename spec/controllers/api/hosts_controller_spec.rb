describe Api::HostsController do
  let(:host) { Host.create(ip: ip) }
  let(:ip) { "8.8.8.8" }
  let(:id) { host.id }

  describe "#create" do
    subject(:perform) { post :create, params: {ip: ip} }

    it "creates host" do
      expect { subject }.to change(Host, :count).by(1)
    end

    context "when host exists" do
      before do
        Host.create(ip: ip)
      end

      it "doesn't create host" do
        expect { subject }.not_to change(Host, :count)
      end

      it "returns error" do
        perform
        expect(response.status).to eq 422
      end
    end

    context "with invalid IP" do
      let(:ip) { "123.333" }

      it "returns error" do
        perform
        expect(response.status).to eq 422
      end

      it "doesn't create host" do
        expect { subject }.not_to change(Host, :count)
      end
    end
  end

  describe "#destory" do
    subject(:perform) { delete :destroy, params: {id: id} }

    it "deletes host" do
      host
      expect { subject }.to change(Host, :count).by(-1)
    end

    context "when id doesn't exist" do
      let(:id) { 0 }

      it "returns 404" do
        perform
        expect(response.status).to eq 404
      end
    end
  end

  describe "stats" do
    subject(:perform) { get :stats, params: {id: id, from: 1.hour.ago, to: Time.current} }

    context "without stat" do
      it "returns error" do
        perform
        expect(response.status).to eq 400
      end
    end

    context "with stats" do
      before do
        host.stats.create(duration: 0.5, received: true, created_at: 2.minutes.ago)
        host.stats.create(duration: 1.0, received: true, created_at: 2.minutes.ago)
        host.stats.create(duration: 1.5, received: true, created_at: 2.minutes.ago)
      end

      it "returns stats" do
        perform
        body = JSON.parse(response.body)
        expect(body).to include "avg_duration" => 1.0
      end
    end
  end
end
