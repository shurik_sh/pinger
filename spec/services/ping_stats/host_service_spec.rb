describe PingStats::HostService do
  describe "ping_hosts" do
    subject { described_class.ping_hosts }

    before do
      Host.create! ip: "8.8.8.8"
      Host.create! ip: "8.7.8.8"
    end

    it "creates stats" do
      expect { subject }.to change(Stat, :count).by(2)
    end
  end
end
