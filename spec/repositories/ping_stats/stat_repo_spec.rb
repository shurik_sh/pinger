describe PingStats::StatRepo do
  describe "stat_by_host" do
    subject { described_class.stat_by_host(host, 1.day.ago, Time.current) }

    let(:host) { Host.create! ip: "8.8.8.8" }

    before do
      host.stats.create(duration: 1.1, received: true)
      host.stats.create(duration: nil, received: false)
      host.stats.create(duration: 5, received: false)
      host.stats.create(duration: 0.50, received: true)
      host.stats.create(duration: 0.1, received: true)
    end

    it "returns aggregations stats" do
      durations = [1.1, 0.5, 0.1]
      avg = MathUtils.average(durations)
      expect(subject).to eq received_percent: ((3 / 5.0) * 100).round(2),
        avg_duration: avg,
        sd_duration: MathUtils.standard_deviation(durations, avg),
        median_duration: MathUtils.median(durations),
        max_duration: 1.1,
        min_duration: 0.1
    end
  end
end
