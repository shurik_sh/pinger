module Api
  class HostsController < ApplicationController
    def index
      render json: Host.pluck(:id, :ip).map { [:id, :ip].zip(_1).to_h }
    end

    def create
      host = Host.new(ip: params.require(:ip))
      if host.save
        render json: host
      else
        render json: {error: host.errors.full_messages}, status: 422
      end
    end

    def destroy
      host = Host.find(params[:id])
      host.destroy!
      head 204
    end

    def stats
      host = Host.find(params[:id])
      from = params.require(:from)
      to = params.require(:to)
      stats = PingStats::StatRepo.stat_by_host(host, from, to)
      if stats
        render json: stats
      else
        render json: {error: "Stats don't exist"}, status: 400
      end
    end
  end
end
