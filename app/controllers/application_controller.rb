class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: :handle_not_found
  rescue_from ActionController::ParameterMissing, with: :handle_bad_params

  private

  def handle_not_found
    render json: {error: "Not found"}, status: 404
  end

  def handle_bad_params
    render json: {error: "Bad params"}, status: 400
  end
end
