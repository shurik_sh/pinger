module PingStats
  class StatRepo
    class << self
      def stat_by_host(host, from, to)
        stats_count = host.stats.where(created_at: from..to).count
        return if stats_count.zero?

        stats = host.stats.where(received: true, created_at: from..to).group(:host_id).pluck(Arel.sql(<<-SQL)).first
          AVG(duration) AS avg,
          MAX(duration) AS max,
          MIN(duration) AS min,
          COUNT(*) AS count,
          array_agg(duration) AS durations
        SQL

        avg, max, min, count, durations = stats

        {
          received_percent: ((count / stats_count.to_f) * 100).round(2),
          avg_duration: avg.round(2),
          sd_duration: MathUtils.standard_deviation(durations, avg),
          median_duration: MathUtils.median(durations),
          max_duration: max,
          min_duration: min
        }
      end
    end
  end
end
