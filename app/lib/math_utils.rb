module MathUtils
  class << self
    def standard_deviation(array, avg)
      variance = array.sum { |i| (i - avg)**2 } / (array.size - 1)
      Math.sqrt(variance).round(2)
    end

    def median(array)
      return if array.size == 0

      tmp = array.sort
      mid = (tmp.size / 2).to_i
      tmp[mid]
    end

    def average(array)
      (array.sum(0.0) / array.size).round(2)
    end
  end
end
