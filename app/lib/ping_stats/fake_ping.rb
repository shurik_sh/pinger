module PingStats
  class FakePing
    attr_accessor :host

    def ping
      @ping = if ENV["FAIL_PINGS"]
        false
      else
        sleep(rand(0..1.5))
        [false, true].sample
      end
    end

    def duration
      rand(100) if @ping
    end
  end
end
