module PingStats
  class Ping
    include Import["ping.driver"]

    Result = Struct.new(:success, :duration) do
      def success?
        success
      end
    end

    def initialize(ip, driver:)
      driver.host = ip
      @driver = driver
    end

    def ping
      Result.new(driver.ping, driver.duration)
    end
  end
end
