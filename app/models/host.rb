class Host < ApplicationRecord
  IP_REGEX = /(?:[0-9]{1,3}\.){3}[0-9]{1,3}/

  has_many :stats

  validates :ip, uniqueness: true, format: {with: IP_REGEX}
end
