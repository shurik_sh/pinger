module PingStats
  class HostService
    class << self
      def ping_hosts
        jobs = []
        Host.all.in_batches do |rel|
          rel.pluck(:id, :ip).each do |id, ip|
            jobs << run_ping(id, ip)
          end
        end
        jobs.each(&:join)
      end

      private

      def run_ping(host_id, ip)
        Thread.new do
          result = PingStats::Ping.new(ip).ping
          Stat.create!(host_id: host_id, duration: result.duration, received: result.success?)
        end
      end
    end
  end
end
