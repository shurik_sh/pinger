init: deps
	touch .devdocker/.bashrc
	touch .devdocker/.irbrc
	touch .devdocker/.pryrc
	lefthook install
	dip bash

deps:
	@which dip &> /dev/null || gem install dip
	@which lefthook &> /dev/null || gem install lefthook
